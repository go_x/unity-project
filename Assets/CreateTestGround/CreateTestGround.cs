using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateTestGround : MonoBehaviour
{


    public Material material;

    // Start is called before the first frame update
    void Start()
    {
        string[,] mapTxtData;
        ReadTextFile("mapText", out mapTxtData);
        MapCell[,] cells = SetMapCell(mapTxtData);
        CreateGroundMesh(cells);
    }


    /// <summary>
    ///  地图名称 txt
    /// </summary>
    string mapTextFileName = "mapText";

    /// <summary>
    /// 读取地图文件 excle里的行数 列数 
    /// </summary>
    /// <param name="mapTextData">地图 二维数组</param>
    public void ReadTextFile(string filePath, out string[,] mapTextData)
    {

        TextAsset txt = Resources.Load<TextAsset>(filePath);
        string fileText = txt.text;
        //去掉回车
        fileText = fileText.Replace("\r\n", "\n");

        string[] fileRows = fileText.Split('\n');

        int rowCount = fileRows.Length;
        int columnCount = fileRows[0].Split('	').Length;
        Debug.Log("rowCount " + rowCount + "   columnCount " + columnCount);


        mapTextData = new string[rowCount, columnCount];

        //读取 txt 
        for (int i = 0; i < rowCount; i++)
        {
            string[] currentRow = fileRows[i].Split('	');
            for (int j = 0; j < currentRow.Length; j++)
            {
                mapTextData[i, j] = currentRow[j];
            }
        }
    }

    MapCell[,] SetMapCell(string[,] mapTxtData)
    {
        //实际的 格子行列数
        int rowCount = mapTxtData.GetLength(0) / 3;
        int columnCount = mapTxtData.GetLength(1) / 3;
        Debug.Log("实际的 格子行列数 rowCount " + rowCount + "   columnCount " + columnCount);

        MapCell[,] cells = new MapCell[rowCount, columnCount];


        for (int i = 1; i < rowCount * 3; i += 3)
        {



            for (int j = 1; j < columnCount * 3; j += 3)
            {

                float[,] cellHeights = new float[3, 3];

                for (int row = 0; row < 3; row++)
                {
                    for (int column = 0; column < 3; column++)
                    {
                        float.TryParse(mapTxtData[i - 1 + row, j - 1 + column], out cellHeights[row, column]);
                    }
                }


                Vector2Int posV2 = new Vector2Int((i - 1) / 3, (j - 1) / 3);
                MapCell mazeCell = new MapCell(cellHeights, posV2);
                cells[(i - 1) / 3, (j - 1) / 3] = mazeCell;
            }

        }

        return cells;
    }

    /// <summary>
    /// 是否创建 高度差产生的边界
    /// </summary>
    bool createBorder = false;

    private GameObject CreateGroundMesh(MapCell[,] cells)
    {
        GameObject root = new GameObject("mapRoot");
        Mesh mesh = new Mesh();

        //存储mesh信息的list
        List<Vector3> vertices = new List<Vector3>();
        List<Vector2> uvs = new List<Vector2>();
        List<Vector3> normals = new List<Vector3>();
        List<int> indices = new List<int>();

        float mapScale = 1f;

        float gridHeight = 0.5f;

        int rowCount = cells.GetLength(0);
        int columnCount = cells.GetLength(1);

        for (int i = 0; i < rowCount; i++)
        {
            for (int j = 0; j < columnCount; j++)
            {
                MapCell mapCell = cells[i, j];

                //当前片 起始索引 就是目前顶点数
                int currentIndices = vertices.Count;
                //↖
                vertices.Add(new Vector3(i, mapCell.heights[0, 0] * gridHeight, j) * mapScale);
                //↗
                vertices.Add(new Vector3(i, mapCell.heights[0, 2] * gridHeight, j + 1) * mapScale);
                //↙
                vertices.Add(new Vector3(i + 1, mapCell.heights[2, 0] * gridHeight, j) * mapScale);
                //↘
                vertices.Add(new Vector3(i + 1, mapCell.heights[2, 2] * gridHeight, j + 1) * mapScale);



                uvs.Add(new Vector2(0, 0));
                uvs.Add(new Vector2(0, 1));
                uvs.Add(new Vector2(1, 0));
                uvs.Add(new Vector2(1, 1));

                //记录mesh索引
                indices.Add(currentIndices);
                indices.Add(currentIndices + 1);
                indices.Add(currentIndices + 2);


                indices.Add(currentIndices + 1);
                indices.Add(currentIndices + 3);
                indices.Add(currentIndices + 2);


            }
        }



        mesh.vertices = vertices.ToArray();
        mesh.triangles = indices.ToArray();
        mesh.uv = uvs.ToArray();

        //重设 法线
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();

        GameObject newMeshObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        newMeshObj.name = "MapObj";
        GameObject.DestroyImmediate(newMeshObj.GetComponent<BoxCollider>());
        newMeshObj.GetComponent<MeshFilter>().mesh = mesh;
        newMeshObj.AddComponent<MeshCollider>();
        newMeshObj.GetComponent<Renderer>().material = material;

        newMeshObj.transform.SetParent(root.transform);

        return root;
    }

}
