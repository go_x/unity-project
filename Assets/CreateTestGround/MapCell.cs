
using UnityEngine;

public class MapCell
{
    public Vector2Int posV2
    {
        get;
        private set;
    }


    public float[,] heights
    {
        get;
        private set;
    }


    /// <summary>
    /// 获取 自身 格子高度
    /// </summary>
    /// <returns></returns>
    public float GetSelfHeight()
    {
        return heights[1, 1];
    }

    public MapCell(float[,] height, Vector2Int posV2)
    {
        this.heights = height;
        this.posV2 = posV2;
    }






}
