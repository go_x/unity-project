using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
// using UnityEditor;

public class CombineMesh
{

    // Dictionary<Material, List<Mesh>> meshByMaterial;
    // Dictionary<string, List<Mesh>> meshByMaterial;

    /// <summary>
    /// 单个mesh 的最大顶点数
    /// </summary>
    static int maxVerticesCount = 65535;



    /// <summary>
    /// 是否存入 tangents
    /// </summary>
    private static bool useTangents = false;

    static Dictionary<string, List<MeshFilter>> meshByMaterial;
    // static GameObject newObjsRoot;

    static Transform currentMaterialParent;


    /// <summary>
    /// 根节点 合并mesh
    /// </summary>
    /// <param name="selections"></param>
    public static void CombineObjsByRoot(Transform[] selections)
    {


        GetAllMeshFilter(selections);
        string combineObjName = "";
        if (selections.Length == 1)
        {
            combineObjName = selections[0].name;
        }

        CreateCobmineMeshObj(combineObjName);
    }



    /// <summary>
    /// 获取所有物体的 meshFilter
    /// </summary>
    private static void GetAllMeshFilter(Transform[] selections)
    {
        meshByMaterial = new Dictionary<string, List<MeshFilter>>();
        meshByMaterial.Clear();
        for (int i = 0; i < selections.Length; i++)
        {
            List<MeshFilter> meshFilters = new List<MeshFilter>();

            Transform currentRoot = selections[i];
            GetComponentsInChildRecursively<MeshFilter>(selections[i], meshFilters);

            //只有可见的物体 才记录
            if (currentRoot.GetComponent<MeshFilter>() && currentRoot.gameObject.activeInHierarchy)
            {
                meshFilters.Add(currentRoot.GetComponent<MeshFilter>());
            }


            foreach (MeshFilter meshFilter in meshFilters)
            {
                string materialName = meshFilter.gameObject.GetComponent<MeshRenderer>().sharedMaterial.name;

                if (!meshByMaterial.ContainsKey(materialName))
                {
                    meshByMaterial.Add(materialName, new List<MeshFilter>());
                }
                else
                {

                }

                if (!meshByMaterial[materialName].Contains(meshFilter))
                {
                    meshByMaterial[materialName].Add(meshFilter);
                }
                else
                {
                    Debug.Log(materialName + " Contains   " + meshFilter.gameObject.name);
                }

            }

        }

    }


    /// <summary>
    /// 单个物品 名字
    /// </summary>
    /// <param name="originalObjRoot"></param>
    static void CreateCobmineMeshObj(string name)
    {

        GameObject newObjsRoot = new GameObject(name + "__combine");
        newObjsRoot.transform.position = Vector3.zero;

        Debug.Log("=========== FindChildMesh  ===============");
        Debug.Log("材质数量 " + meshByMaterial.Count);

        foreach (KeyValuePair<string, List<MeshFilter>> keyPair in meshByMaterial)
        {
            // Debug.Log(" 材质 " + keyPair.Key + "   meshFilter数量 " + keyPair.Value.Count);
            //材质一样的mesh 放到一个父物体下
            currentMaterialParent = new GameObject(keyPair.Key).transform;
            currentMaterialParent.transform.SetParent(newObjsRoot.transform);

            CombineMeshes(keyPair.Value);
        }

    }



    static List<Vector3> vertices;
    static List<Vector2> uvs;
    static List<Vector3> normals;
    static List<int> indices;


    //todo 有的有 有的没有 处理？
    static List<Vector4> tangents;

    static int addMaxIndics = int.MinValue;




    /// <summary>
    ///  开始合并mesh
    /// </summary>
    /// <param name="meshes"></param>
    private static void CombineMeshes(List<MeshFilter> meshes)
    {
        // Debug.Log(" CombineMeshes  meshes.Count   " + meshes.Count);

        ClearMeshAttribute();
        for (int i = 0; i < meshes.Count; i++)
        {
            // Debug.Log(i + "    AddMesh");
            AddMesh(meshes[i]);
        }

        CreateNewMesh(meshes[meshes.Count - 1].GetComponent<MeshRenderer>().sharedMaterial);
    }


    private static void ClearMeshAttribute()
    {
        vertices = new List<Vector3>();
        uvs = new List<Vector2>();
        normals = new List<Vector3>();
        indices = new List<int>();
        // addMaxIndics = 0;

        tangents = new List<Vector4>();
        addMaxIndics = 0;

    }

    private static void AddMesh(MeshFilter meshFilter)
    {

        Material material = meshFilter.GetComponent<MeshRenderer>().sharedMaterial;

        GameObject meshObj = meshFilter.gameObject;

        // Mesh mesh = meshFilter.mesh;
        Mesh mesh = meshFilter.sharedMesh;
        Vector3[] newVertices = mesh.vertices;



        // 每个模型顶点数量不超过 65535 超过就分 下一个
        if (newVertices.Length + vertices.Count >= maxVerticesCount)
        {
            CreateNewMesh(material);
            ClearMeshAttribute();
        }



        Vector2[] newUvs = mesh.uv;
        Vector3[] newNormals = mesh.normals;

        Vector4[] newtangents = mesh.tangents;

        int[] newIndices = mesh.triangles;



        //获取世界坐标的顶点
        /// https://stackoverflow.com/questions/35751886/how-do-i-add-the-rotation-scale-and-translation-of-my-object-to-my-vertex-in-un


        for (int i = 0; i < newVertices.Length; i++)
        {
            vertices.Add(meshObj.transform.TransformPoint(newVertices[i]));


            // transform.rotation*Vector3.Scale(myVector,transform.localScale) + transform.position;
            // var diference = (GO.position - transform.position);
            // var FinalPos = Quaternion.Inverse(transform.rotation) * Vector3(diference.x / transform.lossyScale.x, diference.y / transform.lossyScale.y, diference.z / transform.lossyScale.z);
        }



        Vector3 fixV3 = new Vector3(
                     meshObj.transform.localScale.x < 0 ? -1 : 1,
                     meshObj.transform.localScale.y < 0 ? -1 : 1,
                     meshObj.transform.localScale.z < 0 ? -1 : 1
                 );


        // //法线 反转？
        for (int i = 0; i < newNormals.Length; i++)
        {
            newNormals[i] = new Vector3(
                newNormals[i].x * fixV3.x,
                newNormals[i].y * fixV3.y,
                newNormals[i].z * fixV3.z
            );
        }

        // local scale  是负数 要反转面？
        if (fixV3.x * fixV3.y * fixV3.z < 0)
        {
            for (int i = 0; i < newIndices.Length; i += 3)
            {
                int change = newIndices[i + 1];

                newIndices[i + 1] = newIndices[i + 2];
                newIndices[i + 2] = change;

            }

        }

        uvs.AddRange(newUvs);
        normals.AddRange(newNormals);
        tangents.AddRange(newtangents);



        for (int i = 0; i < newIndices.Length; i++)
        {
            int index = newIndices[i] + addMaxIndics;
            indices.Add(index);

        }

        addMaxIndics = vertices.Count;

    }


    private static void CreateNewMesh(Material material)
    {
        GameObject createMesh = GameObject.CreatePrimitive(PrimitiveType.Cube);

        Mesh mesh = new Mesh();
        GameObject.DestroyImmediate(createMesh.GetComponent<Collider>());


        mesh.vertices = vertices.ToArray();
        mesh.uv = uvs.ToArray();
        mesh.normals = normals.ToArray();
        mesh.triangles = indices.ToArray();

        // mesh.vertices = GetRoundNumbers(vertices.ToArray());
        // mesh.uv = GetRoundNumbers(uvs.ToArray());
        // mesh.normals = GetRoundNumbers(normals.ToArray());
        // mesh.triangles = indices.ToArray();

        if (useTangents)
        {
            mesh.tangents = tangents.ToArray();
        }

        createMesh.GetComponent<MeshFilter>().mesh = mesh;


        // createMesh.gameObject.name = material.name;
        createMesh.GetComponent<MeshRenderer>().sharedMaterial = material;

        createMesh.transform.SetParent(currentMaterialParent, true);
        createMesh.gameObject.name = currentMaterialParent.childCount.ToString();

    }



    // /// <summary>
    // ///小数点位数 用来保留小数点后几位的
    // /// 目前感觉这个对文件大小不起啥作用啊
    // /// </summary>
    // static float decimalPlaces = 100000f;


    // /// <summary>
    // /// 获得保留 decimalPlaces 位小数的数字
    // /// </summary>
    // /// <param name="number"></param>
    // /// <returns></returns>
    // private static float GetRoundNumber(float number)
    // {
    //     return Mathf.Round(number * decimalPlaces) / decimalPlaces;
    // }

    // private static Vector3 GetRoundVector3(Vector3 vector3)
    // {

    //     vector3.x = GetRoundNumber(vector3.x);
    //     vector3.y = GetRoundNumber(vector3.y);
    //     vector3.z = GetRoundNumber(vector3.z);

    //     return vector3;
    // }
    // private static Vector2 GetRoundVector2(Vector2 vector2)
    // {

    //     vector2.x = GetRoundNumber(vector2.x);
    //     vector2.y = GetRoundNumber(vector2.y);

    //     return vector2;
    // }


    // private static Vector3[] GetRoundNumbers(Vector3[] vector3s)
    // {
    //     for (int i = 0; i < vector3s.Length; i++)
    //     {
    //         vector3s[i] = GetRoundVector3(vector3s[i]);
    //     }
    //     return vector3s;

    // }


    // private static Vector2[] GetRoundNumbers(Vector2[] vector2s)
    // {
    //     for (int i = 0; i < vector2s.Length; i++)
    //     {
    //         vector2s[i] = GetRoundVector2(vector2s[i]);
    //     }
    //     return vector2s;

    // }


    // private static float[] GetRoundNumbers(float[] floats)
    // {
    //     for (int i = 0; i < floats.Length; i++)
    //     {
    //         floats[i] = GetRoundNumber(floats[i]);
    //     }
    //     return floats;

    // }




    /// <summary>
    /// 查找组件
    /// </summary>
    /// <param name="_transform"></param>
    /// <param name="target"></param>
    /// <typeparam name="T"></typeparam>
    public static void GetComponentInChildRecursively<T>(Transform _transform, ref T target)
    {

        foreach (Transform t in _transform)
        {
            T[] components = t.GetComponents<T>();

            foreach (T component in components)
            {
                if (component != null)
                {
                    // Debug.Log(" component != null ");

                    target = component;
                    return;
                }
            }

            if (target == null)
            {
                GetComponentInChildRecursively<T>(t, ref target);
            }

        }

    }

    /// <summary>
    /// 查找组件
    /// </summary>
    /// <param name="_transform"></param>
    /// <param name="_componentList"></param>
    /// <typeparam name="T"></typeparam>
    /// <returns></returns>
    private static List<T> GetComponentsInChildRecursively<T>(Transform _transform, List<T> _componentList)
    {

        foreach (Transform t in _transform)
        {
            T[] components = t.GetComponents<T>();

            foreach (T component in components)
            {
                if (component != null)
                {
                    _componentList.Add(component);
                }
            }

            GetComponentsInChildRecursively<T>(t, _componentList);
        }


        return _componentList;
    }



    private GameObject CreateCube(Vector3 point)
    {
        GameObject cubePointObj = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cubePointObj.transform.localScale = new Vector3(0.1f, 0.1f, 0.1f);

        cubePointObj.transform.position = point;
        return cubePointObj;
    }





}
