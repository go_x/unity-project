using System.Collections;
using System.Collections.Generic;
using UnityEngine;



#if UNITY_EDITOR
using UnityEditor;
public class CombineMeshWindow : EditorWindow
{

    // [MenuItem("Tools/合并选择mesh")]
    // static void CombineMesh222()
    // {
    //     GameObject[] objs = Selection.gameObjects;

    //     for (int j = 0; j < objs.Length; j++)
    //     {
    //         MeshFilter[] meshfilters = objs[j].GetComponentsInChildren<MeshFilter>();
    //         CombineInstance[] combine = new CombineInstance[meshfilters.Length];
    //         Matrix4x4 matrix = objs[j].transform.worldToLocalMatrix;

    //         for (int i = 0; i < meshfilters.Length; i++)
    //         {
    //             MeshFilter mf = meshfilters[i];

    //             MeshRenderer mr = mf.GetComponent<MeshRenderer>();

    //             if (mr == null) continue;

    //             combine[i].mesh = mf.sharedMesh;

    //             combine[i].transform = matrix * mf.transform.localToWorldMatrix;
    //         }
    //         Mesh mesh = new Mesh();

    //         mesh.name = objs[j].name;

    //         mesh.CombineMeshes(combine, true);

    //         // string path = @"Assets/Model/Combinemesh/" + mesh.name + ".asset";
    //         string path = @"Assets/Combinemesh/" + mesh.name + ".asset";


    //         AssetDatabase.CreateAsset(mesh, path);
    //     }
    //     AssetDatabase.Refresh();

    //     EditorUtility.DisplayDialog("CombineMesh", "Combine successfully!", "OK", "");
    // }

    // [MenuItem("Window/CombineMesh")]//在unity菜单Window下有MyWindow选项
    [MenuItem("Tools/合并选择mesh")]
    static void Init()
    {
        CombineMeshWindow myWindow = (CombineMeshWindow)EditorWindow.GetWindow(typeof(CombineMeshWindow), false, "CombineMeshEditor", true);//创建窗口
        myWindow.Show();//展示
    }

    bool showToggle = true;
    private void OnGUI()
    {

        GUILayout.Label("当前选中obj数量: " + Selection.transforms.Length);
        // GetSelectionTransform();
        for (int i = 0; i < Selection.transforms.Length; i++)
        {
            GUILayout.Label(Selection.transforms[i].name);
        }


        if (GUILayout.Button("合并选中mesh"))
        {
            if (Selection.transforms.Length == 0)
            {
                GUILayout.Label("ERROR 没有选中的Obj");
            }
            else
            {
                CombineMesh.CombineObjsByRoot(Selection.transforms);
            }
        }

    }


    private void GetSelectionTransform()
    {
        if (Selection.transforms.Length == 0) return;
        for (int i = 0; i < Selection.transforms.Length; i++)
        {
            Transform currentRoot = Selection.transforms[i];
            // Debug.Log(i + "  " + currentRoot.name);
        }

    }
}
#endif