Shader "Custom/VertexOffsetShaders"
{

    Properties
    {
        _Color ("Color", Color) = (1,1,1,1)
        _MainTex ("Albedo (RGB)", 2D) = "white" {}
        _Glossiness ("Smoothness", Range(0,1)) = 0.5
        _Metallic ("Metallic", Range(0,1)) = 0.0
        _AddScaleY("AddScaleY",Range(0,10))=0


        //特效 范围 
        _EffectRange("Effect Range", Vector) = (0, 0, 0, 0)

        
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM
        //阴影问题
        //https://forum.unity.com/threads/surface-shader-wrong-normals-after-vertex-modification.492472/

        // Physically based Standard lighting model, and enable shadows on all light types
        #pragma surface surf Standard fullforwardshadows vertex:vert addshadow

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

        sampler2D _MainTex;

        struct Input
        {
            float2 uv_MainTex;
            float3 worldPos;
            float3 worldNormal;
            float3 viewDir;
        };
        half3 ObjectScale() {
            return half3(
            length(unity_ObjectToWorld._m00_m10_m20),
            length(unity_ObjectToWorld._m01_m11_m21),
            length(unity_ObjectToWorld._m02_m12_m22)
            );
        }

        half _Glossiness;
        half _Metallic;
        fixed4 _Color;

        float _AddScaleY;
        fixed4 _EffectRange;


        // Add instancing support for this shader. You need to check 'Enable Instancing' on materials that use the shader.
        // See https://docs.unity3d.com/Manual/GPUInstancing.html for more information about instancing.
        // #pragma instancing_options assumeuniformscaling
        UNITY_INSTANCING_BUFFER_START(Props)
        // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)

        float4 changePosistion(float4 v){
            float4 world = mul(unity_ObjectToWorld, v);

            // >y最低点  开始需要移动顶点位置
            if(world.y>_EffectRange.x){
                
                //在范围中
                if(world.y<_EffectRange.y){
                    world.y   = world.y +   (world.y - _EffectRange.x)*_AddScaleY;
                    // world.y *= _AddScaleY;
                    // world.xyz = float3(0,0,0);
                    }else{

                    world.y +=  _EffectRange.y*_AddScaleY;
                }
                
            }
            return mul(unity_WorldToObject, world);
            
        }




        void vert(inout appdata_full v)
        {
            v.vertex = changePosistion(v.vertex);
        }
        

        void surf (Input IN, inout SurfaceOutputStandard o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex) * _Color;
            o.Albedo = c.rgb;
            // Metallic and smoothness come from slider variables
            o.Metallic = _Metallic;
            o.Smoothness = _Glossiness;
            o.Alpha = c.a;

        }
        ENDCG
    }
    FallBack "Diffuse"
}
