//https://blog.csdn.net/qq_33457938/article/details/82897490



Shader "Unlit/MeltShader"
{
    Properties{
        _MainTex("Base(rgb)", 2D) = "white"{}
        _Color("_Color",Color) = (1,1,1,1)


        _NoiseMap("NoiseMap", 2D) = "white"{}
        _StartColor("StarColor", Color) = (0,0,0,0)
        _EndColor("EndColor", Color) = (0,0,0,0)

        //melt 阈值
        _MeltThreshold("MeltThreshold", Range(0, 1)) = 0
        _Erode("Erode", Range(0.0, 1.0)) = 0.98
        _ErodeThreshold("ErodeThreshold", Range(0.0, 1.0)) = 0.71



        
        //模型 高度 边界
        // [HideInInspector]
        _MeshBoundTopPosY("MeshBoundTopPosY",Float)=1.0
        // [HideInInspector]
        _MeshBoundBottomPosY("MeshBoundBottomPosY",Float)=1.0

        // [HideInInspector]
        //是否从上到下播放效果
        [Toggle(TOPTOBOTTOM)] _TopToBottom("Top To Bottom", Float) = 0


        //是否 播放 从显示 到隐藏
        // [Toggle(TOPTOBOTTOM)] _TopToBottom("show To Hide", Float) = 0


        

        //消融效果 到消失效果的 offset
        _MeltOffSet("MeltOffSet",Range(0.0, 0.5))=0.2


        // //使用面板控制cull模式
        // [Enum(UnityEngine.Rendering.CullMode)] _Cull("Cull Mode", Float) = 0

        _FlowDirection ("Flow Direction", Vector) = (0, 1, 0, 0)
    }

    SubShader
    {
        Tags
        {
            "RenderType"="Transparent" 
            "Queue" = "Geometry"
            "IgnoreProjector" = "True"
        }
        LOD 100

        Pass
        {
            ZTest On
            ZWrite On
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Back

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"

            sampler2D _MainTex;
            fixed4 _Color;


            float4 _MainTex_ST;
            sampler2D _NoiseMap;
            //消融边缘起始颜色
            fixed4 _StartColor;
            //最终颜色
            fixed4 _EndColor;
            //消融阈值
            float _MeltThreshold;


            //控制侵蚀程度
            //todo
            float _Erode;
            float _ErodeThreshold;

            float _MeshBoundTopPosY;
            float _MeshBoundBottomPosY;
            float _MeltOffSet;
            Vector _FlowDirection;


            float _TopToBottom;


            struct a2v{
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float4 texcoord : TEXCOORD0;
            };

            struct v2f{
                float4 pos : SV_POSITION;
                float3 worldNormal : TEXCOORD0;
                float3 worldPos : TEXCOORD1;
                float2 uv : TEXCOORD2;

                //AutoLight.cginc
                // SHADOW_COORDS(3)
            };


            //假随机 网上找的
            float random (float2 uv)
            {
                return frac(sin(dot(uv,float2(12.9898,78.233)))*43758.5453123);
            }
            

            //常规的顶点着色器 坐标转换
            v2f vert(a2v v){
                v2f o;
                o.pos = UnityObjectToClipPos(v.vertex);
                o.worldNormal = UnityObjectToWorldNormal(v.normal);
                //模型坐标系-世界坐标系
                o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
                //纹理坐标
                o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
                //AutoLight.cginc
                // TRANSFER_SHADOW(o);
                return o;
            }

            fixed4 frag(v2f i) : SV_Target{

                //颜色
                fixed4 albedo = tex2D(_MainTex, i.uv)*_Color;

                if(_MeltThreshold<=0||_MeltThreshold>=1){
                    return albedo;
                }

                //从值来看 renderer消失线 posY
                float meltLinePosY =  _MeshBoundBottomPosY + (_MeshBoundTopPosY -_MeshBoundBottomPosY) * _MeltThreshold;

                //特效 范围 值
                float offsetY = (_MeshBoundTopPosY-_MeshBoundBottomPosY) * _MeltOffSet;

                //特效 范围 偏移量 [meltLinePosY,topEffectPosY]
                
                //开始消融特效的位置
                float bottomEffectPosY = meltLinePosY  - offsetY * 0.5;

                //结束消融特效的位置
                float topEffectPosY = meltLinePosY  + offsetY * 0.5;


                


                //显示的百分比
                float showPercent =0.0;

                //还没到 需要特效位置

                // if(i.worldPos.y < bottomEffectPosY ){
                    //     showPercent=0;
                // }
                // else   if( bottomEffectPosY < i.worldPos.y && i.worldPos.y < topEffectPosY ){
                    //     showPercent  =( i.worldPos.y  - meltLinePosY)/offsetY; 
                // }

                // //已经过了 特效展示位置
                // else if(i.worldPos.y > topEffectPosY ){
                    //     showPercent=1;
                    //     }else{
                    
                // }


                if(i.worldPos.y < bottomEffectPosY ){
                    showPercent=0;
                }
                else if(i.worldPos.y > topEffectPosY ){
                    showPercent=1;
                    }else{
                    showPercent  =( i.worldPos.y  - meltLinePosY)/offsetY;   
                }
                //已经过了 特效展示位置
                
                

                //取图位置  noiseOffset 偏移量
                float noiseSpeedX = 0;
                float noiseSpeedY = 4;

                float2 noiseOffset = float2( _Time.x*noiseSpeedX ,_Time.x*noiseSpeedY);
                fixed3 melt = tex2D(_NoiseMap, i.uv + noiseOffset ).rgb;
                

                // //取图 消失范围  震荡 0 - 1
                // float waveSpeed =20.0;
                // float strength = 0.5+sin(_Time.y * waveSpeed )*0.3 ;

                // if(showPercent<1&&showPercent>0){
                    //     showPercent*=strength;
                // }



                if(melt.r<=0){
                    melt.r=0.01;
                }
                
                if(melt.r>=1){
                    melt.r=0.99;
                }
                
                clip(melt.r - showPercent );
                // clip(melt.r * melt.r - showPercent );

                return albedo;



                
                
                // // else 
                // if (i.worldPos.y < topEffectPosY){

                    
                    //     //使用噪声图，伪随机数
                    //     // fixed3 melt = tex2D(_NoiseMap, i.uv).rgb;
                    //     float noiseSpeed = 5;
                    //     // float2 noiseOffset = float2( _FlowDirection.x * _Time.x*noiseSpeed,_FlowDirection.y * _Time.x*noiseSpeed);

                    //     float2 noiseOffset = float2(  _Time.x *noiseSpeed ,_Time.x*noiseSpeed*2);
                    //     // fixed3 melt = tex2D(_NoiseMap, i.uv + noiseOffset ).rgb;

                    //     // fixed3 melt  = tex2D(_NoiseMap, random(i.uv) ).rgb;

                    //     // fixed3 melt  = tex2D(_NoiseMap, i.uv + (random(i.uv)- float2(0.5,0.5))*0.2f  ).rgb;

                    //     fixed3 melt  = tex2D(_NoiseMap, i.uv ).rgb;

                    //     float myFade = 1 - (i.worldPos.y - meltLinePosY)/offsetY;
                    //     clip(melt.r - myFade );



                    //     // if(i.worldPos.y < meltLinePosY){
                        //         //     discard;
                    //     // }
                    //     // return fixed4(0,1,0,1);
                    //     // //采样阈值与设定阈值比较，小于设定的阈值就裁剪掉该片元，不作渲染
                    //     // clip(melt.r - _MeltThreshold);
                    
                    //     // //取 消融阈值与噪声值的占比判断消融的侵蚀程度
                    //     // // float result = _MeltThreshold / melt.r;
                    //     // float result = myFade / melt.r;

                    //     // if(result > _Erode){
                        //         //     //如果结果大于消融颜色的阈值，则返回消融结束部分的颜色，否则返回初始颜色
                        //         //     if(result > _ErodeThreshold) 
                        //         //     return _EndColor;
                        //         //     return _StartColor;
                    //     // }
                    //     // return fixed4(albedo, 1);
                // }
                
                // return fixed4(albedo, 1);
                return albedo;

            }
            ENDCG
        }
    }
}



// Shader ""
// {
    //     Properties{
        //         _MainTex("Base(rgb)", 2D) = "white"{}
        //         _NoiseMap("NoiseMap", 2D) = "white"{}
        //         _StartColor("StarColor", Color) = (0,0,0,0)
        //         _EndColor("EndColor", Color) = (0,0,0,0)
        //         _MeltThreshold("MeltThreshold", Range(0, 1)) = 0
        //         _Erode("Erode", Range(0.0, 1.0)) = 0.98
        //         _ErodeThreshold("ErodeThreshold", Range(0.0, 1.0)) = 0.71


        
        //         //模型 高度 边界
        //         // [HideInInspector]
        //         _MeshBoundTopPosY("MeshBoundTopPosY",Float)=1.0
        //         // [HideInInspector]
        //         _MeshBoundBottomPosY("MeshBoundBottomPosY",Float)=1.0


        //         //消融效果 到消失效果的 offset
        //         _MeltOffSet("MeltOffSet",Range(0.0, 0.5))=0.2


        //         // //使用面板控制cull模式
        //         // [Enum(UnityEngine.Rendering.CullMode)] _Cull("Cull Mode", Float) = 0

        //         _FlowDirection ("Flow Direction", Vector) = (0, 1, 0, 0)
    //     }

    //     SubShader{

        //         CGINCLUDE

        //         #include "Lighting.cginc"
        //         #include "UnityCG.cginc"
        //         // #include "AutoLight.cginc"

        //         sampler2D _MainTex;
        //         float4 _MainTex_ST;
        //         sampler2D _NoiseMap;
        //         //消融边缘起始颜色
        //         fixed4 _StartColor;
        //         //最终颜色
        //         fixed4 _EndColor;
        //         //消融阈值
        //         float _MeltThreshold;
        //         //控制侵蚀程度
        //         float _Erode;
        //         float _ErodeThreshold;

        //         float _MeshBoundTopPosY;
        //         float _MeshBoundBottomPosY;
        //         float _MeltOffSet;
        //         Vector _FlowDirection;


        //         struct a2v{
            //             float4 vertex : POSITION;
            //             float3 normal : NORMAL;
            //             float4 texcoord : TEXCOORD0;
        //         };

        //         struct v2f{
            //             float4 pos : SV_POSITION;
            //             float3 worldNormal : TEXCOORD0;
            //             float3 worldPos : TEXCOORD1;
            //             float2 uv : TEXCOORD2;

            //             //AutoLight.cginc
            //             // SHADOW_COORDS(3)
        //         };

        //         float random (float2 uv)
        //         {
            //             return frac(sin(dot(uv,float2(12.9898,78.233)))*43758.5453123);
        //         }
        

        //         //常规的顶点着色器 坐标转换
        //         v2f vert(a2v v){
            //             v2f o;
            //             o.pos = UnityObjectToClipPos(v.vertex);
            //             o.worldNormal = UnityObjectToWorldNormal(v.normal);
            //             //模型坐标系-世界坐标系
            //             o.worldPos = mul(unity_ObjectToWorld, v.vertex).xyz;
            //             //纹理坐标
            //             o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
            //             //AutoLight.cginc
            //             // TRANSFER_SHADOW(o);
            //             return o;
        //         }

        //         fixed4 frag(v2f i) : SV_Target{

            //             //反射率
            //             fixed3 albedo = tex2D(_MainTex, i.uv).rgb;

            
            
            
            //             if(_MeltThreshold>0){
                //                 float meltLinePosY =  _MeshBoundTopPosY +(_MeshBoundBottomPosY-_MeshBoundTopPosY)* _MeltThreshold;

                //                 float offsetY = (_MeshBoundBottomPosY-_MeshBoundTopPosY)* _MeltOffSet;
                //                 //特效 偏移量
                //                 float topEffectPosY = meltLinePosY +offsetY;

                //                 if(i.worldPos.y > meltLinePosY){
                    //                     discard;
                //                 }
                //                 return fixed4(albedo,1);
                
                
                
                //                 // else 
                //                 if (i.worldPos.y < topEffectPosY){

                    
                    
                    //                     //使用噪声图，伪随机数
                    //                     // fixed3 melt = tex2D(_NoiseMap, i.uv).rgb;
                    //                     float noiseSpeed=5;
                    //                     // float2 noiseOffset = float2( _FlowDirection.x * _Time.x*noiseSpeed,_FlowDirection.y * _Time.x*noiseSpeed);

                    //                     float2 noiseOffset = float2(  _Time.x *noiseSpeed ,_Time.x*noiseSpeed*2);


                    //                     // fixed3 melt = tex2D(_NoiseMap, i.uv + noiseOffset ).rgb;

                    //                     // fixed3 melt  = tex2D(_NoiseMap, random(i.uv) ).rgb;

                    //                     // fixed3 melt  = tex2D(_NoiseMap, i.uv + (random(i.uv)- float2(0.5,0.5))*0.2f  ).rgb;

                    //                     fixed3 melt  = tex2D(_NoiseMap, i.uv ).rgb;


                    
                    //                     // return float4(melt,1);

                    //                     // float myFade = sin(_Time.x*100)/3.14;
                    //                     // myFade *= (i.worldPos.y - meltLinePosY)/_MeltOffSet;

                    //                     float myFade =1- (i.worldPos.y - meltLinePosY)/offsetY;

                    //                     clip(melt.r - myFade );

                    //                     // if(i.worldPos.y < meltLinePosY){
                        //                         //     discard;
                    //                     // }

                    
                    
                    //                     // return fixed4(0,1,0,1);
                    //                     // //采样阈值与设定阈值比较，小于设定的阈值就裁剪掉该片元，不作渲染
                    //                     // clip(melt.r - _MeltThreshold);
                    
                    //                     // //取 消融阈值与噪声值的占比判断消融的侵蚀程度
                    //                     // // float result = _MeltThreshold / melt.r;
                    //                     // float result = myFade / melt.r;

                    //                     // if(result > _Erode){
                        //                         //     //如果结果大于消融颜色的阈值，则返回消融结束部分的颜色，否则返回初始颜色
                        //                         //     if(result > _ErodeThreshold) 
                        //                         //     return _EndColor;
                        //                         //     return _StartColor;
                    //                     // }
                    //                     // return fixed4(albedo, 1);
                //                 }
                
            //             }  
            //             return fixed4(albedo, 1);
        //         }

        //         ENDCG


        //         Pass{

            //             Tags{"RenderType" = "Opaque"}
            //             // Cull[_Cull]
            //             Cull Off
            //             CGPROGRAM
            
            //             #pragma vertex vert
            //             #pragma fragment frag

            //             ENDCG
        //         }
    //     }

    //     // FallBack Off
    //     FallBack "Diffuse"
// }