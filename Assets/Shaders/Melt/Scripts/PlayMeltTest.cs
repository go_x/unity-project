﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System;
using System.Reflection;



#if UNITY_EDITOR
[CustomEditor(typeof(PlayMeltTest))]
public class Inspector : Editor
{
    PlayMeltTest meltTest;
    private void OnEnable()
    {
        meltTest = (target as PlayMeltTest);
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        if (GUILayout.Button("Auto Get Renderers And Reset Materials"))
        {
            meltTest.FindMaterials();
            meltTest.GetBoundingBox();
        }

    }
}
#endif



//编辑模式下 测试用

[ExecuteAlways]
public class PlayMeltTest : MonoBehaviour
{



    // public Material material;


    /// <summary>
    /// 目标物体
    /// </summary>
    public GameObject targetObj;

    public Shader meltShader;
    public Texture noiseTexture;




    // public List<Renderer> renderers = new List<Renderer>();
    public Renderer[] renderers;
    public List<Material> materials = new List<Material>();


    [Range(0.01f, 1.0f)]
    public float meltSpeed = 0.2f;




    /// <summary>
    /// 显示
    /// </summary>
    private float meltThreshold = 0.0f;


    /// <summary>
    /// 重选中 materials
    /// </summary>
    public bool reset = false;



    /// <summary>
    /// 播放一次特效
    /// </summary>
    public bool startPlayEffect = false;

    private bool isPlayingEffect = false;



    //消融效果 到消失效果的 offset
    [Range(0f, 0.5f)]
    public float meltOffSet = 0f;



    /// <summary>
    /// 显示到隐藏
    /// </summary>
    public bool showToHide = false;


    /// <summary>
    /// 是否从上到下播放效果
    /// </summary>
    public bool topToBottom = true;



    void Update()
    {

        if (reset)
        {
            ToClearConsole();

            if (materials.Count == 0)
            {
                FindMaterials();

            }
            ResetMaterialState();
            reset = false;

            //停止
            isPlayingEffect = false;
            startPlayEffect = false;

        }

        if (startPlayEffect)
        {

            if (!isPlayingEffect)
            {

                StartPlayEffect();


            }

        }

        if (isPlayingEffect)
        {
            PlayMeltEffect();
        }

    }


    /// <summary>
    /// 开始播放特效
    /// </summary>
    private void StartPlayEffect()
    {
        //停止
        isPlayingEffect = false;

        if (renderers == null || renderers.Length == 0)
        {
            Debug.LogError("renderers == null || renderers.Length == 0");
            startPlayEffect = false;
            return;
        }

        ResetMaterialState();
        GetBoundingBox();

        //set 参数
        foreach (Material material in materials)
        {
            material.SetFloat("_MeshBoundTopPosY", topBoundsPosY);
            // material.SetFloat("_MeshBoundBottomPosY", bottomPosY  );
            material.SetFloat("_MeshBoundBottomPosY", bottomBoundsPosY - meltOffSet);

            material.SetFloat("_MeltOffSet", meltOffSet);



            material.SetFloat("_TopToBottom", topToBottom ? 1.0f : 0.0f);
        }

        Debug.Log("_TopToBottom   " + (topToBottom ? 1.0f : 0.0f));




        // startPlayEffect = false;
        isPlayingEffect = true;
    }


    /// <summary>
    /// 重设材质
    /// </summary>

    public void FindMaterials()
    {

        renderers = targetObj.GetComponentsInChildren<Renderer>();
        materials.Clear();

        foreach (Renderer renderer in renderers)
        {

            foreach (Material material in renderer.sharedMaterials)
            {
                //目标shader name
                if (!material.shader.name.Contains("MeltShader"))
                {

                    Debug.LogWarning(renderer.gameObject.name + " 材质不是shader ");
                    // continue;
                    // // // //TODO 不应该 直接替换？
                    material.shader = meltShader;
                    material.SetTexture("_NoiseMap", noiseTexture);


                }


                if (!materials.Contains(material))
                {

                    materials.Add(material);
                }
            }

        }

        Debug.Log(" materials count " + materials.Count);
        if (materials.Count == 0)
        {
            Debug.LogError("materials.Count == 0");
        }
        ResetMaterialState();
    }



    /// <summary>
    /// bounding 顶部位置
    /// </summary>
    public float topBoundsPosY;

    /// <summary>
    /// bounding 底部位置
    /// </summary>
    public float bottomBoundsPosY;


    /// <summary>
    /// 获取 所有renderer 上下 范围
    /// </summary>
    public void GetBoundingBox()
    {

        topBoundsPosY = float.MinValue;
        bottomBoundsPosY = float.MaxValue;

        foreach (Renderer renderer in renderers)
        {
            Bounds bounds = renderer.bounds;
            float rendererTopPosY = bounds.center.y + bounds.size.y / 2;
            float rendererBottomPosY = bounds.center.y - bounds.size.y / 2;


            topBoundsPosY = rendererTopPosY > topBoundsPosY ? rendererTopPosY : topBoundsPosY;
            bottomBoundsPosY = rendererBottomPosY < bottomBoundsPosY ? rendererBottomPosY : bottomBoundsPosY;

        }

        Debug.Log(" topPosY  " + topBoundsPosY + "  bottomPosY " + bottomBoundsPosY);

    }

    /// <summary>
    /// 重设 材质 状态
    /// </summary>
    void ResetMaterialState()
    {
        meltThreshold = showToHide ? 0 : 1;
        playTime = 0;

        foreach (Material material in materials)
        {
            material.SetFloat("_MeltThreshold", meltThreshold);
        }
        isPlayingEffect = false;
    }

    float playTime = 0;

    void PlayMeltEffect()
    {

        // meltThreshold = Mathf.Repeat(Time.deltaTime * meltSpeed, 6.0f);

        if (showToHide)
        {
            meltThreshold += Time.deltaTime * meltSpeed;
            if (meltThreshold > 1)
            {
                meltThreshold = 1;

                //over
                isPlayingEffect = false;
                startPlayEffect = false;

            }

        }
        else
        {
            meltThreshold -= Time.deltaTime * meltSpeed;
            if (meltThreshold < 0)
            {
                meltThreshold = 0;

                //over
                isPlayingEffect = false;
                startPlayEffect = false;

            }

        }

        foreach (Material material in materials)
        {
            material.SetFloat("_MeltThreshold", meltThreshold);
        }

    }


    /// <summary>
    /// 编辑器 update
    /// </summary>
    private void OnDrawGizmos()
    {
#if UNITY_EDITOR
        // Ensure continuous Update calls.
        if (!Application.isPlaying)
        {
            UnityEditor.EditorApplication.QueuePlayerLoopUpdate();
            UnityEditor.SceneView.RepaintAll();
        }
#endif
    }



    private void ToClearConsole()
    {
        Assembly assembly = Assembly.GetAssembly(typeof(SceneView));
        System.Type type = assembly.GetType("UnityEditor.LogEntries");
        MethodInfo method = type.GetMethod("Clear");
        method.Invoke(null, null);
    }


}



// [InitializeOnLoad]

// public static class EditorHotkeysTracker
// {
//     static EditorHotkeysTracker()
//     {

//         Debug.Log("   Initialize    EditorHotkeysTracker  ");
//         SceneView.duringSceneGui += view =>
//        {
//            var e = Event.current;
//            if (e != null && e.keyCode != KeyCode.None)
//                Debug.Log("Key pressed in editor: " + e.keyCode);
//        };
//     }
// }