using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using Sprites = UnityEngine.Sprites;

#if UNITY_EDITOR
using UnityEditor;

[CustomEditor(typeof(CenterFixedImage), true)]
[CanEditMultipleObjects]
public class CircletImageEditor : UnityEditor.UI.ImageEditor
{
    //固定区域 x:left y:bottom z:right w:top
    //X = left, Y = bottom, Z = right, W = top.
    SerializedProperty _centerArea;
    protected override void OnEnable()
    {
        base.OnEnable();
        _centerArea = serializedObject.FindProperty("centerArea");
    }
    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        serializedObject.Update();
        // EditorGUILayout.Vector4Field("Point 4 ", _centerArea);
        EditorGUILayout.PropertyField(_centerArea);
        // EditorGUILayout.Slider(_leftCenter, 0f, 0.5f, new GUIContent("leftCenter"));
        // EditorGUILayout.Slider(_rightCenter, 0.5f, 1f, new GUIContent("rightCenter"));
        serializedObject.ApplyModifiedProperties();
        if (GUI.changed)
        {
            EditorUtility.SetDirty(target);
        }
    }
}
#endif

/// <summary>
/// 
/// </summary>
public class CenterFixedImage : Image
{

    /// <summary>
    /// 中心区域
    /// X = left, Y = bottom, Z = right, W = top.
    /// </summary>
    [SerializeField]
    Vector4 centerArea;

    /// <summary>
    /// Update the UI renderer mesh.
    /// </summary>
    protected override void OnPopulateMesh(VertexHelper toFill)
    {
        //重写 slice 状态
        if (type == Type.Sliced)
        {
            GenerateSlicedSprite(toFill);
            return;
        }


        base.OnPopulateMesh(toFill);
        //false 图像会拉伸
        // GenerateSimpleSprite(toFill, false);
    }
    private Sprite activeSprite { get { return overrideSprite != null ? overrideSprite : sprite; } }


    /// <summary>
    /// Generate vertices for a simple Image.
    /// </summary>
    void GenerateSimpleSprite(VertexHelper vh, bool lPreserveAspect)
    {

        //     // v2 ---- v3
        //     // |       |
        //     // v1 ---- v4  
        //     Vector3 v1 = new Vector3(v.x, v.y);
        //     Vector3 v2 = new Vector3(v.x, v.w);
        //     Vector3 v3 = new Vector3(v.z, v.w);
        //     Vector3 v4 = new Vector3(v.z, v.y);

        Vector4 v = GetDrawingDimensions(lPreserveAspect);
        var uv = (activeSprite != null) ? Sprites.DataUtility.GetOuterUV(activeSprite) : Vector4.zero;

        var color32 = color;
        vh.Clear();
        vh.AddVert(new Vector3(v.x, v.y), color32, new Vector2(uv.x, uv.y));
        vh.AddVert(new Vector3(v.x, v.w), color32, new Vector2(uv.x, uv.w));
        vh.AddVert(new Vector3(v.z, v.w), color32, new Vector2(uv.z, uv.w));
        vh.AddVert(new Vector3(v.z, v.y), color32, new Vector2(uv.z, uv.y));

        vh.AddTriangle(0, 1, 2);
        vh.AddTriangle(2, 3, 0);
    }
    /// Image's dimensions used for drawing. X = left, Y = bottom, Z = right, W = top.
    private Vector4 GetDrawingDimensions(bool shouldPreserveAspect)
    {
        var padding = activeSprite == null ? Vector4.zero : UnityEngine.Sprites.DataUtility.GetPadding(activeSprite);
        var size = activeSprite == null ? Vector2.zero : new Vector2(activeSprite.rect.width, activeSprite.rect.height);

        Rect r = GetPixelAdjustedRect();

        int spriteW = Mathf.RoundToInt(size.x);
        int spriteH = Mathf.RoundToInt(size.y);

        var v = new Vector4(
            padding.x / spriteW,
            padding.y / spriteH,
            (spriteW - padding.z) / spriteW,
            (spriteH - padding.w) / spriteH);

        if (shouldPreserveAspect && size.sqrMagnitude > 0.0f)
        {
            PreserveSpriteAspectRatio(ref r, size);
        }

        v = new Vector4(
            r.x + r.width * v.x,
            r.y + r.height * v.y,
            r.x + r.width * v.z,
            r.y + r.height * v.w
        );

        return v;
    }
    private void PreserveSpriteAspectRatio(ref Rect rect, Vector2 spriteSize)
    {
        var spriteRatio = spriteSize.x / spriteSize.y;
        var rectRatio = rect.width / rect.height;

        if (spriteRatio > rectRatio)
        {
            var oldHeight = rect.height;
            rect.height = rect.width * (1.0f / spriteRatio);
            rect.y += (oldHeight - rect.height) * rectTransform.pivot.y;
        }
        else
        {
            var oldWidth = rect.width;
            rect.width = rect.height * spriteRatio;
            rect.x += (oldWidth - rect.width) * rectTransform.pivot.x;
        }
    }

    /// <summary>
    /// 5*5 slice
    /// </summary>
    static readonly Vector2[] s_VertScratch = new Vector2[6];
    static readonly Vector2[] s_UVScratch = new Vector2[6];

    /// <summary>
    /// Generate vertices for a 9-sliced Image.
    /// </summary>
    private void GenerateSlicedSprite(VertexHelper toFill)
    {
        if (!hasBorder)
        {
            GenerateSimpleSprite(toFill, false);
            return;
        }
        Vector4 outer, inner, padding, border;

        if (activeSprite != null)
        {
            outer = Sprites.DataUtility.GetOuterUV(activeSprite);
            inner = Sprites.DataUtility.GetInnerUV(activeSprite);
            padding = Sprites.DataUtility.GetPadding(activeSprite);
            border = activeSprite.border;
        }
        else
        {
            outer = Vector4.zero;
            inner = Vector4.zero;
            padding = Vector4.zero;
            border = Vector4.zero;
        }


        //中心区域 应该比编辑器的区域小
        if (centerArea.x < inner.x ||
            centerArea.y < inner.y ||
            centerArea.z > inner.z ||
            centerArea.w > inner.w)
        {
            Debug.LogWarning("centerArea is out of inner");
        }


        Rect rect = GetPixelAdjustedRect();

        Vector4 adjustedBorders = GetAdjustedBorders(border / multipliedPixelsPerUnit, rect);
        padding = padding / multipliedPixelsPerUnit;


        //起点终点
        s_VertScratch[0] = new Vector2(padding.x, padding.y);
        s_VertScratch[5] = new Vector2(rect.width - padding.z, rect.height - padding.w);
        //中心位置
        Vector2 centerPos = (s_VertScratch[5] - s_VertScratch[0]) / 2f;


        //.9开始拉伸位置
        s_VertScratch[1].x = adjustedBorders.x;
        s_VertScratch[1].y = adjustedBorders.y;

        s_VertScratch[4].x = rect.width - adjustedBorders.z;
        s_VertScratch[4].y = rect.height - adjustedBorders.w;

        //中心区域 不变位置
        s_VertScratch[2].x = centerPos.x - sprite.texture.width * (0.5f - centerArea.x);
        s_VertScratch[2].y = centerPos.y - sprite.texture.height * (0.5f - centerArea.y);

        s_VertScratch[3].x = centerPos.x + sprite.texture.width * (centerArea.z - 0.5f);
        s_VertScratch[3].y = centerPos.y + sprite.texture.height * (centerArea.w - 0.5f);



        for (int i = 0; i < s_VertScratch.Length; ++i)
        {
            s_VertScratch[i].x += rect.x;
            s_VertScratch[i].y += rect.y;
        }

        s_UVScratch[0] = new Vector2(outer.x, outer.y);
        s_UVScratch[1] = new Vector2(inner.x, inner.y);

        s_UVScratch[4] = new Vector2(inner.z, inner.w);
        s_UVScratch[5] = new Vector2(outer.z, outer.w);

        s_UVScratch[2] = new Vector2(centerArea.x, centerArea.y);
        s_UVScratch[3] = new Vector2(centerArea.z, centerArea.w);

        toFill.Clear();

        for (int x = 0; x < s_UVScratch.Length - 1; ++x)
        {
            int x2 = x + 1;
            for (int y = 0; y < s_UVScratch.Length - 1; ++y)
            {

                if (!fillCenter && x == 2 && y == 2)
                    continue;
                int y2 = y + 1;
                AddQuad(toFill,
                    new Vector2(s_VertScratch[x].x, s_VertScratch[y].y),
                    new Vector2(s_VertScratch[x2].x, s_VertScratch[y2].y),
                    color,
                    new Vector2(s_UVScratch[x].x, s_UVScratch[y].y),
                    new Vector2(s_UVScratch[x2].x, s_UVScratch[y2].y));
            }
        }

    }

    private Vector4 GetAdjustedBorders(Vector4 border, Rect adjustedRect)
    {
        Rect originalRect = rectTransform.rect;

        for (int axis = 0; axis <= 1; axis++)
        {
            float borderScaleRatio;

            // The adjusted rect (adjusted for pixel correctness)
            // may be slightly larger than the original rect.
            // Adjust the border to match the adjustedRect to avoid
            // small gaps between borders (case 833201).
            if (originalRect.size[axis] != 0)
            {
                borderScaleRatio = adjustedRect.size[axis] / originalRect.size[axis];
                border[axis] *= borderScaleRatio;
                border[axis + 2] *= borderScaleRatio;
            }

            // If the rect is smaller than the combined borders, then there's not room for the borders at their normal size.
            // In order to avoid artefacts with overlapping borders, we scale the borders down to fit.
            float combinedBorders = border[axis] + border[axis + 2];
            if (adjustedRect.size[axis] < combinedBorders && combinedBorders != 0)
            {
                borderScaleRatio = adjustedRect.size[axis] / combinedBorders;
                border[axis] *= borderScaleRatio;
                border[axis + 2] *= borderScaleRatio;
            }
        }
        return border;
    }

    static void AddQuad(VertexHelper vertexHelper, Vector2 posMin, Vector2 posMax, Color32 color, Vector2 uvMin, Vector2 uvMax)
    {
        int startIndex = vertexHelper.currentVertCount;

        vertexHelper.AddVert(new Vector3(posMin.x, posMin.y, 0), color, new Vector2(uvMin.x, uvMin.y));
        vertexHelper.AddVert(new Vector3(posMin.x, posMax.y, 0), color, new Vector2(uvMin.x, uvMax.y));
        vertexHelper.AddVert(new Vector3(posMax.x, posMax.y, 0), color, new Vector2(uvMax.x, uvMax.y));
        vertexHelper.AddVert(new Vector3(posMax.x, posMin.y, 0), color, new Vector2(uvMax.x, uvMin.y));

        vertexHelper.AddTriangle(startIndex, startIndex + 1, startIndex + 2);
        vertexHelper.AddTriangle(startIndex + 2, startIndex + 3, startIndex);
    }

}
