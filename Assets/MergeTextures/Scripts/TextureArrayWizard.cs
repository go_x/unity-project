﻿
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
public class TextureArrayWizard : ScriptableWizard
{

    public Texture2D[] textures;

    [MenuItem("Tools/Create/Texture Array")]
    static void CreateWizard()
    {
        // ScriptableWizard.DisplayWizard<TextureArrayWizard>(
        //     "Create Texture Array", "Create"
        // );


        ScriptableWizard.DisplayWizard<TextureArrayWizard>("Create Light", "Create");
    }

    private void OnWizardCreate()
    {
        if (textures.Length == 0)
        {
            return;
        }

        //打开一个保存文件面板
        string path = EditorUtility.SaveFilePanelInProject(
         "Save Texture Array", "Texture Array", "asset", "Save Texture Array"
     );




        if (path.Length == 0)
        {
            errorString = "没有选择预制体";
            return;
        }
        helpString = "textures.Length" + textures.Length;

        Texture2D t = textures[0];
        Texture2DArray textureArray = new Texture2DArray(
            t.width, t.height, textures.Length, t.format, t.mipmapCount > 1
        );

        textureArray.anisoLevel = t.anisoLevel;
        textureArray.filterMode = t.filterMode;
        textureArray.wrapMode = t.wrapMode;

        for (int i = 0; i < textures.Length; i++)
        {
            for (int m = 0; m < t.mipmapCount; m++)
            {
                Graphics.CopyTexture(textures[i], 0, m, textureArray, i, m);
            }
        }

        AssetDatabase.CreateAsset(textureArray, path);
    }

}
#endif