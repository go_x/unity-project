using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class GetMaterialFromScene : EditorWindow
{
    [MenuItem("Tools/Get Materials in scene")]
    static void Init()
    {
        EditorWindow window = GetWindow(typeof(GetMaterialFromScene));
        window.Show();
    }

    private void OnGUI()
    {
        List<Texture2D> textures = new List<Texture2D>(Selection.GetFiltered<Texture2D>(SelectionMode.Unfiltered));
        foreach (Texture texture in textures)
        {
            if (texture != null)
            {
                string path = AssetDatabase.GetAssetPath(texture);
                string containingFolder = null;
                if (string.IsNullOrEmpty(containingFolder))
                {
                    containingFolder = path.Remove(path.LastIndexOf('/'));
                }
                var importer = AssetImporter.GetAtPath(path) as TextureImporter;
                List<Sprite> currentFrames = new List<Sprite>();
                int index = 0;
                foreach (SpriteMetaData spriteMetaData in importer.spritesheet)
                {
                    string[] slice = spriteMetaData.name.Split('_');
                    if (slice[2] != index.ToString())
                    {
                        //CreateAnimation(currentFrames.Count, currentFrames);
                        currentFrames = new List<Sprite>();
                        index++;
                    }

                    // The Code works fine until here, I need the "Sprite" type object to set up the animation
                    Sprite sprite = AssetDatabase.LoadAssetAtPath<Sprite>($"{containingFolder}/{spriteMetaData.name}");
                    //currentFrames.Add(sprite);
                    Debug.Log(sprite.name);
                }
            }
        }
    }
}